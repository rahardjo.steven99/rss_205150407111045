package com.example.rss_205150407111045.Interface;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);

}
